// Importación (requerimiento) de los paquetes
const express = require('express');
const morgan = require('morgan');
const app = express(); //ejecución del framework


// settings
app.set('port', process.env.PORT || 3000); //establefce el número de puerto
/**
 * process.env.PORT || 3000:    En caso de que en el sistema exista un puerto definido
 *                              en el sistema (o en el serv. de la nube) [suele ser un puerto definido]
 *                              que lo tome. sino que use el puerto 3000 por defecto. Valida si un puerto existe.
 */

 app.set('json spaces', 2); // pinta "bonito" los json


// Starting the middleware
// Revisa las peticiones antes de que llegue al servidor
/**
 * MORGAN MODES:
 * - dev: Shows HTTP-METHOD ROUTE SERVER-RESPONSE TIME MS - BYTES-SIZE
 * - combined: Shows DATE - "HTTP-METHOD ROUTE HTTP-PROTOCOL-VERSION" SERVER-RESPONSE TIME "CLIENT-BROWSER-INFO" 
 */
app.use(morgan('dev'));

/**
 * Dado que el servidor va estar respondiendo
 * vamos a estar enviando archivos JSON. 
*/
app.use(express.urlencoded({extended: false})); // Para los datos enviados con HTML/CSS/JS usando formularios
                                                // extended:false : para entender datos de los forms. 
                                                            //Entiende textos sencillos, no media
app.use(express.json()); //Permite al servidor recivir y entender los formato JSON



/**
 *  R O U T E S
 */
// Movido a routes/
app.use(require('./routes/index')); // Importa la ruta creada en otro archivo
app.use("/api/movies",require('./routes/movies'));
app.use("/api/users", require("./routes/users"));


// Starting the Server
app.listen(app.get('port'), () => {
    console.log(`Listening on Port ${app.get('port')}`);
})
