const {Router} = require('express');
const router = Router();

// CONSUMIENDO DATOS DE OTRO SERVICIO
const fetch = require('node-fetch'); // para hacer peticiones HTTP con node

router.get("/", async (req, res) => { // async -> necesario para await
    const response = await fetch('https://jsonplaceholder.typicode.com/users') // await : como es petición asíncrona, lo va a esperar para continuar
    
    // response se trata de un strin, no de un JSON
    // lo convertimos a JSON
    const users = await response.json();
    // console.log(users);
    res.json(users);
});

module.exports = router;