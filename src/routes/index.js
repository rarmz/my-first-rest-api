/**
 * Exportar las rutas para que funcione en otro archivo
 * que lo requiera.
 * 
 * Router: método que permite definir nuevas rutas para el servidor
 */
const { Router } = require('express');
const router = Router();



/**
 *  R O U T E S
 */

 // Desde la app, cuando visiten la ruta inicial ("/") responde con algo
 // Para que la ruta sea reconocida en otro archivo, ahora se define como un enrutados / router
router.get("/", (req,res) => {
    // res.send({"Title": "Hello World"}); responde texto plano, que el browser puede interpretar como HTML
    res.json({"Title" : "Hello World"});
});


/**
 * Ruta que responde un objeto
 */
router.get("/new-route", (req,res) => {
    const data = {
        "Title": "Testing",
        "Caption" : "New Route",
        "Message" : "Grettings!"
    };

    res.json(data);
});

module.exports = router; // Exporta la ruta y podrá ser usado en otro archivo