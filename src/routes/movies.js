const { Router } = require('express');
const router = Router();
const _ = require('underscore');

const movies = require("../sample.json");

// Obtener todas las movies que hay en el servidor
router.get("/", (req,res) => {
    res.json(movies);
})

// Guardar una nueva movie en el servidor
router.post("/", (req, res) => {
    //console.log(req.body); // lee el contenido enviado al servidor (en formato JSON)
    //res.send('Recibido!'); // Respondemos al cliente
    const { title, director, year, rating } = req.body;

    /*Validamos que todos los datos existan */
    if ( title && director && year && rating){
        // res.send("All fine")

        /** GENERANDOD UN NUEVO ID PAR EL NUEVO OBJETO */
        id = movies.length + 1;

        const newMovie = {...req.body, id}; // "id" agrega la propiedad "id" con valor al nuevo objeto
        console.log(newMovie);
        movies.push(newMovie); // inserta un nuevo objeto en el array de "movies"

        res.json(movies);

    } else {
        res.status(500).send("Something is wrong, check your data");
    }
});


// Eliminar una movie del servidor 
router.delete("/:id", (req, res) => {

   const { id } = req.params;
    // Uso de underscore
    // Each, recorre el array
    _.each(movies, (movie, i) => {
        console.log(movie);
        if (movie.id == id){ 
            /*console.log("ID: " + (id));
            console.log("Actual index: " + i);
            console.log("Movie id: " + movie.id);*/
            movies.splice(i,1);
            res.json(movies);
        }   
    });
    //res.send("Deleted")
    //res.json(movies);

});


// Actualizar una movie del servidor
router.put("/:id", (req, res) => {
    const { id } = req.params;

    const { title, director, year, rating } = req.body;

    /*Validamos que todos los datos existan */
    if ( title && director && year && rating){
        _.each(movies, (movie,i) => {
            if(movie.id == id){
                movie.title = title;
                movie.director = director;
                movie.year = year;
                movie.rating = rating;
            }
        });

        res.json(movies);
    } else {
        res.status(500).json({"Error": "Some data is wrong or is absent. Check your data"});
    }
})

module.exports = router;